package us.fetchr.calllogger.constants;

import us.fetchr.calllogger.dtos.RecordingDTO;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class InputTests {

  public static RecordingDTO mockRecording =
      new RecordingDTO(
          1,
          "Driver1",
          "0567324740",
          "0567324741",
          "#1",
          "https://s3-ap-southeast-1.amazonaws.com/calllogger.s3.fetchr.us/audio/christina_NA_1475424918513863605.mp3",
          0,
          LocalDateTime.now(),
          LocalDateTime.now(),
          0,
          31.200092,
          29.918739,
          "attended",
          "Outgoing");

  public static RecordingDTO newRecording =
      new RecordingDTO(
          2,
          "Driver12",
          "0567324740",
          "0567324741",
          "#1",
          "https://s3-ap-southeast-1.amazonaws.com/calllogger.s3.fetchr.us/audio/christina_NA_1475424918513863605.mp3",
          0,
          LocalDateTime.now(),
          LocalDateTime.now(),
          0,
          31.200092,
          29.918739,
          "attended",
          "Outgoing");

  public static RecordingDTO updatedMockRecording =
      new RecordingDTO(
          1,
          "Driver12",
          "0567324740",
          "0567324741",
          "#1",
          "https://s3-ap-southeast-1.amazonaws.com/calllogger.s3.fetchr.us/audio/christina_NA_1475424918513863605.mp3",
          0,
          LocalDateTime.now(),
          LocalDateTime.now(),
          0,
          31.200092,
          29.918739,
          "attended",
          "Outgoing");

  public static List<RecordingDTO> mockRecordingsList = Arrays.asList(mockRecording);
}
