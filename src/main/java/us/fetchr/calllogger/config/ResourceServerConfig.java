package us.fetchr.calllogger.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

  @Autowired private Environment environment;

  @Override
  public void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers(HttpMethod.OPTIONS, "/v1.0/recordings/**")
        .permitAll()
        .antMatchers("/v1.0/recordings/**")
        .access("#oauth2.hasScope('calllogger')");
    http.csrf().disable();
    http.httpBasic().disable();
  }

  @Bean
  @Primary
  public RemoteTokenServices tokenServices() {
    final RemoteTokenServices remoteTokenServices = new RemoteTokenServices();
    remoteTokenServices.setCheckTokenEndpointUrl(getCheckTokenEndPointUrl());
    remoteTokenServices.setClientId(getClientId());
    remoteTokenServices.setClientSecret(getClientSecret());
    return remoteTokenServices;
  }

  public String getClientSecret() {
    return environment.getProperty("sso.client.secret", "");
  }

  public String getClientId() {
    return environment.getProperty("sso.client.id", "");
  }

  public String getCheckTokenEndPointUrl() {
    return environment.getProperty("oauth.checktoken.endpoint.uri", "");
  }
}
