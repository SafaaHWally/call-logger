package us.fetchr.calllogger.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import us.fetchr.calllogger.constants.InputTests;
import us.fetchr.calllogger.dtos.RecordingDTO;
import us.fetchr.calllogger.models.Recording;
import us.fetchr.calllogger.repositories.RecordingRepository;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class RecordingServiceTest {

  @Autowired private RecordingsService recordingsService;

  @Autowired private RecordingRepository recordingRepository;

  private Recording recordingFromDb;

  @Before
  public void setupTest() {
    recordingFromDb =
        recordingRepository.save(RecordingDTO.convertToEntity(InputTests.mockRecording));
  }

  @Test
  public void createRecording() throws JsonProcessingException {

    recordingsService.createRecording(InputTests.mockRecording);

    Recording recordingFromDb = recordingRepository.findOne(InputTests.mockRecording.getId());
    ObjectMapper objectMapper = new ObjectMapper();
    String expected = objectMapper.writeValueAsString(InputTests.mockRecording);
    String actual = objectMapper.writeValueAsString(recordingFromDb);

    assertEquals(expected, actual);
  }

  @Test
  public void getRecordings() {

    List<RecordingDTO> recordingsListFromDb =
        recordingsService.getRecordings(null, null, null, null);
    assertEquals(InputTests.mockRecordingsList, recordingsListFromDb);
  }

  @Test
  public void searchRecordingBySalesOrder() {

    List<RecordingDTO> recordingsListFromDb = recordingsService.getRecordingsBySalesOrder("#1");
    assertEquals(InputTests.mockRecordingsList, recordingsListFromDb);
  }

  @Test
  public void searchRecordingById() {

    RecordingDTO recordingsFromDb =
        recordingsService.getRecordingById(InputTests.mockRecording.getId());
    assertEquals(InputTests.mockRecording, recordingsFromDb);
  }

  @Test
  public void updateRecording() {

    RecordingDTO recordingsFromDb =
        recordingsService.updateRecording(
            InputTests.updatedMockRecording, InputTests.mockRecording.getId());
    assertEquals(InputTests.updatedMockRecording, recordingsFromDb);
  }

  @Test
  public void deleteRecording() {

    recordingRepository.save(RecordingDTO.convertToEntity(InputTests.newRecording));
    try {
      recordingsService.deleteRecording(InputTests.newRecording.getId());
      List<RecordingDTO> recordingsDto =
          recordingRepository.findAll().stream()
              .map(recording -> RecordingDTO.convertToDto(recording))
              .collect(Collectors.toList());
      assertEquals(InputTests.mockRecordingsList, recordingsDto);
    } catch (Exception e) {

    }
  }
}
