package us.fetchr.calllogger.swagger;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class Swagger {
  @Bean
  public Docket api() {
    ArrayList<ApiKey> keys = new ArrayList<>();
    keys.add(new ApiKey("Authorization", "Bearer", "header"));
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        // .paths(Predicates.not(PathSelectors.regex("/error")))
        .apis(RequestHandlerSelectors.basePackage("us.fetchr.calllogger.controllers"))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(getApiInfo())
        .securitySchemes(keys);
  }

  private ApiInfo getApiInfo() {
    return new ApiInfo(
        "Call Logging service",
        "Call Logging Service is a data management service using Spring Boot and Musql. The main functionality of this service is to work as an interface with MongoDB to fetch, insert, update, or delete the  data.",
        "2.0",
        "TERMS OF SERVICE URL",
        new Contact("Safaa Wally", "", "safaa.w@fetchr.us"),
        "LICENSE",
        "LICENSE URL");
  }
}
