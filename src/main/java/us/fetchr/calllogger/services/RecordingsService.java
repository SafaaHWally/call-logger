package us.fetchr.calllogger.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import us.fetchr.calllogger.constants.Constants;
import us.fetchr.calllogger.dtos.RecordingDTO;
import us.fetchr.calllogger.models.Recording;
import us.fetchr.calllogger.repositories.RecordingRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RecordingsService {

  @Autowired private RecordingRepository recordingRepository;

  public RecordingDTO createRecording(RecordingDTO recording) {
    Recording model = recordingRepository.save(RecordingDTO.convertToEntity(recording));
    return RecordingDTO.convertToDto(model);
  }

  public List<RecordingDTO> getRecordings(
      String driverId, LocalDate startDate, LocalDate endDate, Integer page) {
    Sort sort = new Sort(Sort.Direction.DESC, Constants.START_TIME);
    Pageable pageable = new PageRequest((page != null) ? page : 0, Constants.PAGE_SIZE, sort);
    List<Recording> recordings =
        recordingRepository.findAllByStartTimeGreaterThanEqualAndEndTimeLessThanEqualAndDriverId(
            startDate != null ? startDate.atStartOfDay() : null,
            endDate != null ? endDate.atStartOfDay() : null,
            driverId,
            pageable);
    List<RecordingDTO> recordingsDto =
        recordings.stream()
            .map(recording -> RecordingDTO.convertToDto(recording))
            .collect(Collectors.toList());
    return recordingsDto;
  }

  public List<RecordingDTO> getRecordingsBySalesOrder(String salesOrder) {
    List<Recording> recordings = recordingRepository.findAllBySalesOrder(salesOrder);
    List<RecordingDTO> recordingsDto =
        recordings.stream()
            .map(recording -> RecordingDTO.convertToDto(recording))
            .collect(Collectors.toList());
    return recordingsDto;
  }

  public RecordingDTO getRecordingById(Long recordingId) {
    Recording recording = recordingRepository.findOne(recordingId);
    return RecordingDTO.convertToDto(recording);
  }

  public RecordingDTO updateRecording(RecordingDTO updatedRecording, Long recordingId) {

    Recording mainRecording = recordingRepository.findOne(recordingId);

    if (mainRecording != null) {
      updatedRecording.setId(recordingId);
      recordingRepository.save(RecordingDTO.convertToEntity(updatedRecording));
      return updatedRecording;
    }

    return RecordingDTO.convertToDto(mainRecording);
  }

  public void deleteRecording(Long recordingId) throws Exception {

    recordingRepository.delete(recordingId);
  }
}
