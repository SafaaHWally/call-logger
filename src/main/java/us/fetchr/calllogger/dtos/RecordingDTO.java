package us.fetchr.calllogger.dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.BeanUtils;
import us.fetchr.calllogger.models.Recording;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordingDTO {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	private String driverId;

	@NotNull
	private String driverNumber;

	@NotNull
	private String customerNumber;

	@NotNull
	private String salesOrder;

	@URL
	private String recording;

	@PositiveOrZero
	private double duration;

	@NotNull
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime startTime;

	@NotNull
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime endTime;

	@PositiveOrZero
	private int count;

	@NotNull
	private double longitude;

	@NotNull
	private double latitude;

	private String status;

	private String type;

	public static Recording convertToEntity(RecordingDTO recordingDto) {
		Recording recording = new Recording();
		BeanUtils.copyProperties(recordingDto, recording);
		return recording;

	}

	public static RecordingDTO convertToDto(Recording recording) {
		RecordingDTO recordingDto = new RecordingDTO();
		BeanUtils.copyProperties(recording, recordingDto);
		return recordingDto;


	}
}
