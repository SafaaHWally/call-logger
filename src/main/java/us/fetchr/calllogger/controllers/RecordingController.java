package us.fetchr.calllogger.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import us.fetchr.calllogger.dtos.RecordingDTO;
import us.fetchr.calllogger.services.RecordingsService;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

// @CrossOrigin
@RestController
@RequestMapping("/v1.0/recordings")
public class RecordingController {

  @Autowired RecordingsService recordingsService;

  @PostMapping
  public ResponseEntity<RecordingDTO> create(@Valid @RequestBody RecordingDTO recording) {
    return new ResponseEntity<>(recordingsService.createRecording(recording), HttpStatus.CREATED);
  }

  @GetMapping
  public ResponseEntity<List<RecordingDTO>> filterRecordings(
      String driverId,
      @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startDate,
      @RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate endDate,
      Integer page) {
    List<RecordingDTO> recordings =
        recordingsService.getRecordings(driverId, startDate, endDate, page);
    return recordings.isEmpty()
        ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(recordings, HttpStatus.OK);
  }

  @RequestMapping(value = "/search", method = RequestMethod.GET)
  public ResponseEntity<List<RecordingDTO>> getRecordingsBySalesOrder(String salesOrder) {
    List<RecordingDTO> recordings = recordingsService.getRecordingsBySalesOrder(salesOrder);
    return recordings.isEmpty()
        ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(recordings, HttpStatus.OK);
  }

  @GetMapping("/{id}")
  public ResponseEntity<RecordingDTO> getRecordingById(
      @PathVariable(value = "id") Long recordingId) {

    RecordingDTO recording = recordingsService.getRecordingById(recordingId);
    return recording == null
        ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(recording, HttpStatus.OK);
  }

  @PutMapping("/{id}")
  public ResponseEntity<RecordingDTO> updateRecording(
      @RequestBody RecordingDTO updatedRecording, @PathVariable(value = "id") Long recordingId) {

    RecordingDTO recording = recordingsService.updateRecording(updatedRecording, recordingId);
    return recording == null
        ? new ResponseEntity<>(HttpStatus.NO_CONTENT)
        : new ResponseEntity<>(recording, HttpStatus.OK);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<String> deleteRecording(@PathVariable(value = "id") Long recordingId) {
    try {
      recordingsService.deleteRecording(recordingId);
      return new ResponseEntity<>("Recording has been delete successfully", HttpStatus.OK);

    } catch (Exception e) {
      return new ResponseEntity<>("There is no recording with this id", HttpStatus.NO_CONTENT);
    }
  }
}
