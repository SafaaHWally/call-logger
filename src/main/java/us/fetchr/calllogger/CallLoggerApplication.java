package us.fetchr.calllogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallLoggerApplication {

  public static void main(String[] args) {
    SpringApplication.run(CallLoggerApplication.class, args);
  }
}
