package us.fetchr.calllogger.repositories;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import us.fetchr.calllogger.models.Recording;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface RecordingRepository extends JpaRepository<Recording, Long> {

  @Query(
      "SELECT rec FROM Recording rec WHERE "
          + "(:driverId IS NULL OR rec.driverId = :driverId) "
          + "and (:startTime IS NULl OR rec.startTime >= :startTime)"
          + "and (:endTime IS NULL OR rec.endTime <= :endTime)")
  List<Recording> findAllByStartTimeGreaterThanEqualAndEndTimeLessThanEqualAndDriverId(
      @Param("startTime") LocalDateTime startTime,
      @Param("endTime") LocalDateTime endTime,
      @Param("driverId") String driverId,
      Pageable pageable);

  List<Recording> findAllBySalesOrder(String salesOrder);
}
