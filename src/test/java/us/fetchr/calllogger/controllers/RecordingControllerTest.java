package us.fetchr.calllogger.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import us.fetchr.calllogger.CallLoggerApplication;
import us.fetchr.calllogger.constants.Constants;
import us.fetchr.calllogger.constants.InputTests;
import us.fetchr.calllogger.dtos.RecordingDTO;
import us.fetchr.calllogger.services.RecordingsService;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = CallLoggerApplication.class)
@WebMvcTest(controllers = RecordingController.class, secure = false)
public class RecordingControllerTest {

  @Autowired MockMvc mockMvc;

  @MockBean RecordingsService recordingsService;

  private String mockRecordingAsString;

  private String mockRecordingListAsString;

  @Before
  public void setupTest() throws JsonProcessingException {
    ObjectMapper objectMapper = new ObjectMapper();
    mockRecordingAsString = objectMapper.writeValueAsString(InputTests.mockRecording);
    mockRecordingListAsString = "[" + mockRecordingAsString + "]";
  }

  @Test
  public void getRecordings() throws Exception {

    Mockito.when(
            recordingsService.getRecordings(
                Mockito.anyString(), Mockito.anyObject(), Mockito.anyObject(), Mockito.anyInt()))
        .thenReturn(InputTests.mockRecordingsList);

    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.get(Constants.RECORDINGS_URI)
            .param("page", "0")
            .accept(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    JSONAssert.assertEquals(
        mockRecordingListAsString, result.getResponse().getContentAsString(), false);
  }

  @Test
  public void createRecording() throws Exception {

    Mockito.when(recordingsService.createRecording(Mockito.any(RecordingDTO.class)))
        .thenReturn(InputTests.mockRecording);

    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.post(Constants.RECORDINGS_URI)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mockRecordingAsString);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();

    MockHttpServletResponse response = result.getResponse();

    assertEquals(mockRecordingAsString, response.getContentAsString());
  }

  @Test
  public void searchRecordingBySalesOrder() throws Exception {

    Mockito.when(recordingsService.getRecordingsBySalesOrder(Mockito.anyString()))
        .thenReturn(InputTests.mockRecordingsList);

    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.get(Constants.RECORDINGS_URI + Constants.SEARCH_URI)
            .param("salesOrder", "#1")
            .accept(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(mockRecordingListAsString, response.getContentAsString());
  }

  @Test
  public void searchRecordingById() throws Exception {

    Mockito.when(recordingsService.getRecordingById(Mockito.anyLong()))
        .thenReturn(InputTests.mockRecording);

    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.get(Constants.RECORDINGS_URI + "/1")
            .accept(MediaType.APPLICATION_JSON);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(mockRecordingAsString, response.getContentAsString());
  }

  @Test
  public void updateRecording() throws Exception {

    Mockito.when(
            recordingsService.updateRecording(Mockito.any(RecordingDTO.class), Mockito.anyLong()))
        .thenReturn(InputTests.mockRecording);

    RequestBuilder requestBuilder =
        MockMvcRequestBuilders.put(Constants.RECORDINGS_URI + "/1")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(mockRecordingAsString);

    MvcResult result = mockMvc.perform(requestBuilder).andReturn();
    MockHttpServletResponse response = result.getResponse();

    assertEquals(mockRecordingAsString, response.getContentAsString());
  }
}
