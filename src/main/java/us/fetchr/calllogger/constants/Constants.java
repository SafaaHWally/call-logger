package us.fetchr.calllogger.constants;

import org.springframework.stereotype.Component;

@Component
public final class Constants {

  public static final String RECORDINGS_URI = "/v1.0/recordings";

  public static final String SEARCH_URI = "/search";

  public static final int PAGE_SIZE = 4;

  public static final String START_TIME = "startTime";

  public static final String ID = "id";
}
